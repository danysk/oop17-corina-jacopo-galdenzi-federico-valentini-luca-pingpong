package src.controller;

import java.awt.Point;
import java.util.Optional;

import src.model.GameConstant;
import src.model.Racket;
import src.utilities.Direction;
import src.view.ViewManager;
/**
 * 
 * @author Luca Valentini
 *
 */
public class RacketsMovementImpl implements RacketsMovement {
    private Racket player1;
    private Optional<Racket> player2;
    private ViewManager theView;
    private boolean upPlayer1;
    private boolean downPlayer1;
    private boolean upPlayer2;
    private boolean downPlayer2;
    private boolean stopped;
    private boolean suspended;
    /**
     * it is the contructor method used when playing 1 vs cpu.
     * @param player1 is the left racket controlled by human player
     * @param theView represents the view of the game
     */
    public RacketsMovementImpl(final Racket player1, final ViewManager theView) {
        this.player1 = player1;
        this.theView = theView;
        this.upPlayer1 = false;
        this.downPlayer1 = false;
        this.downPlayer2 = false;
        this.upPlayer2 = false;
        this.player2 = Optional.empty();
        this.stopped = false;
        this.suspended = false;
    }
    /**
     * it is the contructor method used when playing 1 vs 1.
     * @param player1 is the left racket controlled by human player
     * @param player2 is the right racket controlled by human player
     * @param theView represents the view of the game
     */
    public RacketsMovementImpl(final Racket player1, final Racket player2, final ViewManager theView) {
        this.player1 = player1;
        this.theView = theView;
        this.upPlayer1 = false;
        this.downPlayer1 = false;
        this.downPlayer2 = false;
        this.upPlayer2 = false;
        this.player2 = Optional.of(player2);
        this.stopped = false;
    }
    @Override
    public final void setUpPlayer1(final boolean upPlayer1) {
        this.upPlayer1 = upPlayer1;
    }
    @Override
    public final void setDownPlayer1(final boolean downPlayer1) {
        this.downPlayer1 = downPlayer1;
    }
    @Override
    public final void setDownPlayer2(final boolean downPlayer2) {
        this.downPlayer2 = downPlayer2;
    }
    @Override
    public final void setUpPlayer2(final boolean upPlayer2) {
        this.upPlayer2 = upPlayer2;
    }
    @Override
    public final void run() {
        while (!stopped) {
            try {
                if (this.downPlayer1) {
                    Point p;
                    p = player1.move(Direction.DOWN);
                    theView.getMatchZone().movePlayer(p, 1);
                }
                if (this.upPlayer1) {
                    Point p;
                    p = player1.move(Direction.UP);
                    theView.getMatchZone().movePlayer(p, 1);
                }
                if (this.player2.isPresent() && this.downPlayer2) {
                    Point p;
                    p = player2.get().move(Direction.DOWN);
                    theView.getMatchZone().movePlayer(p, 2);
                }
                if (this.player2.isPresent() && this.upPlayer2) {
                    Point p;
                    p = player2.get().move(Direction.UP);
                    theView.getMatchZone().movePlayer(p, 2);
                }
                Thread.sleep(GameConstant.WAITING_TIME_PLAYER);
                synchronized (this) {
                    if (suspended) {
                       wait();
                    }
                 }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public final synchronized void resumeThread() {
        suspended = false;
        notify();
    }
    @Override
    public final void pauseThread() {
        suspended = true;
    }
    @Override
    public final synchronized void stopThread() {
       this.stopped = true;
       notify();
    }
}
