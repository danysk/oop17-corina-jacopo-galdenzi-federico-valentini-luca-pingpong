package src.controller;
/**
 * 
 * this class allow to stop, pause and resume a thread.
 *
 */
public interface ThreadManagement {
    /**
     * the execution of the thread is resumed.
     */
    void resumeThread();
    /**
     * the execution of the thread is paused.
     */
    void pauseThread();
    /**
     * the execution of the thread is stopped.
     */
    void stopThread();
}
