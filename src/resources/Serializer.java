package src.resources;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import src.model.Score;

/**
 * This class contains functions useful to save match scores.
 * 
 * @author Jacopo Corina
 *
 */
public class Serializer {
    /**
     * Saves the score through the standard output stream.If already exists a
     * previous score history, the content is loaded,new score is added an then
     * saved.Otherwise,a new file is created with the score argument.
     * 
     * @param object
     *            The score to be saved
     * @param inputStream
     *            the input stream of the file
     * 
     */
    public void saveScore(final Score object, final InputStream inputStream) {
        List<Score> list = null;
        ObjectOutputStream mainOutputStream = null;
        if (inputStream == null) {
            list = new ArrayList<>();
        } else {
            list = getScoreHistory(inputStream);
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        list.add(object);

        try {
            mainOutputStream = new ObjectOutputStream(ResourcesManagement.getScoreHistoryOutputStream());
            mainOutputStream.writeObject(list);
            mainOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 
     * @param inputStream
     *            inputstream of the score file
     * @return List of scores saved
     */
    @SuppressWarnings("unchecked")
    public List<Score> getScoreHistory(final InputStream inputStream) {
        List<Score> list = null;
        if (inputStream != null) {
            try {
                ObjectInputStream in = new ObjectInputStream(inputStream);
                list = (List<Score>) in.readObject();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
                return null;
            } catch (ClassNotFoundException ex) {
                System.out.println(ex.getMessage());
            }

        }
        return list;
    }

}
