package src.utilities;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

/**
 * This class contains some utility functions.
 * @author Jacopo Corina
 *
 */
public final class Utility {
    private static final int SIZE_OF_FONT = 50;
    private Utility() {

    }
    /**
     * value of a second in ms.
     */
    public static final int MSEC_CONST = 1000;
    /**
     * Allows to load a font from file.
     * @param inputStream The path of the font to load
     * @return The font object
     */
    public static Font fontLoader(final InputStream inputStream) {
        Font font = null;
        try {
            font = Font.createFont(Font.TRUETYPE_FONT, inputStream);
            font = font.deriveFont(Font.PLAIN, Utility.SIZE_OF_FONT);

        } catch (IOException | FontFormatException e) {
            //JOptionPane.showMessageDialog(null,e.getMessage());
        }
        return font;
    }
    /**
     * Generates a random integer between min (inclusive) and max (inclusive).
     * @param min
     * @param max
     * @return a random number included in the given range
     */

    /**
     * 
     * @param min minimum
     * @param max maximum
     * @return a random number between minimum and maximum
     */
    public static int getRandomNumberInRange(final int min, final int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }


}
