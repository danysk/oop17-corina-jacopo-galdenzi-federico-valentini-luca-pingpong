package src.utilities;
/**
 * direction that can be used.
 */
public enum Direction { UP, DOWN, LEFT, RIGHT };
