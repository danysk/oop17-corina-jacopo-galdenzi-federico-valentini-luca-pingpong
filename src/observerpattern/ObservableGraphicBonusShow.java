package src.observerpattern;
/**
 * 
 * @author Jacopo Corina
 *
 */
public interface ObservableGraphicBonusShow {
    /**
     * 
     * @param obs the observer on which will be applied bonus showing actions
     */
    void setGraphicBonusShowObserver(GraphicBonusShowObserver obs);
}
