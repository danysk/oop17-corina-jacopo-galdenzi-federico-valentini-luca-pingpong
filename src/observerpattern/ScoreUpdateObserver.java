package src.observerpattern;
/**
 * 
 * @author Jacopo Corina
 *
 */
public interface ScoreUpdateObserver {
    /**
     * 
     * @param playerNumber the number of the player which makes goal.
     */
    void executeOnScoreUpdate(int playerNumber);
}
