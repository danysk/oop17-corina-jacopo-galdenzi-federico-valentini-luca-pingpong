package src.observerpattern;
/**
 * 
 * @author Jacopo Corina
 *
 */
public interface ObservableExpiringBonusTimer {
    /**
     * 
     * @param obs the observer which will be notified when a bonus timer expires
     */
    void setExpiringBonusTimerObserver(ExpiringBonusTimerObserver obs); 
    /**
     * notifies the setted observer when a bonus timer expires.
     */
    void notifyExpiringBonusTimerObserver();
}
