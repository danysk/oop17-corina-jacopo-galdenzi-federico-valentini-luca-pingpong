package src.model;

import src.observerpattern.ExpiringBonusTimerObserver;
/**
 * 
 * @author Federico Galdenzi
 * This class rappresents the speed bonus.
 * when the bonus is applied to the ball it will increment of a constant the X speed.
 * it has no behaviour to handle.
 */
public class SpeedBonus extends BonusTimer {
    /**
     * This is the constructor of the bonus timer which you have to pass the parameters.
     * @param ball the ball object
     * @param observer the ExpiringBonusTimerObserver object, in this case the BonusManager
     * @param interval the interval of the timer
     * @param duration the duration of the timer
     */
    public SpeedBonus(final Ball ball, final ExpiringBonusTimerObserver observer, final int interval, final int duration) {
        super(ball, observer, interval, duration);
    }
    @Override
    public final void applyStrategy() {
        this.start();
        this.getBall().getProprierty().addToSpeed(GameConstant.BONUS_SPEED_X, 0);
    }

    @Override
    public final void resetStrategy() {
        this.getBall().getProprierty().addToSpeed(-GameConstant.BONUS_SPEED_X, 0);
    }

    @Override
    public final void handleStrategy() {
        return;
    }

}
