package src.model;


/**
 * This class allows to manage a match status.
 *
 * @author Jacopo Corina
 */
public class ScoreImpl implements Score {
    private static final long serialVersionUID = -3930055289816320974L;
    private String player1Name;
    private String player2Name;
    private int score1;
    private int score2;
    private int max;
    /**
     * Instantiates a new ScoreImpl.
     *
     * @param s the ScoreImpl to be 
     */
    public ScoreImpl(final ScoreImpl s) {
        this.player1Name = new String(s.getPlayer1Name());
        this.player2Name = new String(s.getPlayer2Name());
        this.score1 = s.getScorePlayer1();
        this.score2 = s.getScorePlayer2();
        this.max = s.getMaximumScore();
    }

    /**
     * Instantiates a new score impl.
     */
    public ScoreImpl() {
        max = GameConstant.VALUE_NOT_SET;
        player1Name = GameConstant.DEFAULT_P1_NAME;
        player2Name = GameConstant.DEFAULT_P2_NAME;
    }
    @Override
    public final void setPlayer1Name(final String player1Name) {
        this.player1Name = player1Name;
    }
    @Override
    public final void setPlayer2Name(final String player2Name) {
        this.player2Name = player2Name;
    }
    /**
     * Instantiates a new score impl.
     *
     * @param n1 the n 1
     * @param n2 the n 2
     */
    public ScoreImpl(final String n1, final String n2) {
        this();
        player1Name = new String(n1);
        player2Name = new String(n2);
    }
    /**
     * Instantiates a new score impl.
     *
     * @param n1 the n 1
     * @param n2 the n 2
     * @param n the n
     */
    public ScoreImpl(final String n1, final String n2, final int n) {
        this(n1, n2);
        max = n;
    }
    /**
     * Instantiates a new score impl.
     *
     * @param n the n
     */
    public ScoreImpl(final int n) {
        this();
        max = n;
    }
    @Override
    public final int getScorePlayer1() {
        return score1;
    }
    @Override
    public final int getScorePlayer2() {
        return score2;
    }
    @Override
    public final void addScorePlayer1() {
        score1++;
    }
    @Override
    public final void addScorePlayer2() {
        score2++;
    }

    /**
     * Returns who is the actual winner: 1 is for player 1, 2 is for player 2, 0 is
     * for even score.
     *
     * @return the winner
     */
    @Override
    public int getWinner() {
        if (score1 > score2) {
            return 1;
        } else if (score1 < score2) {
            return 2;
        } else {
            return 0;
        }
    }

    /**
     * if maximum score limit is set,checks if somebody has reached it. If a maximum
     * score is not set returns -1;
     *
     * @return the winner with maximum
     */
    @Override
    public int getWinnerWithMaximum() {
        if (max != GameConstant.VALUE_NOT_SET) {
            if (score1 == max || score2 == max) {
                return getWinner();
            }
        }
        return GameConstant.VALUE_NOT_SET;
    }
    @Override
    public final void reset() {
        score1 = 0;
        score2 = 0;
    }

    @Override
    public final String toString() {
        return this.getPlayer1Name() + ":" + this.getScorePlayer1() + " - " + this.getPlayer2Name() + ":" + this.getScorePlayer2();
    }

    @Override
    public final String getPlayer1Name() {
        return player1Name;
    }

    @Override
    public final String getPlayer2Name() {
        return player2Name;
    }

    private int getMaximumScore() {
        return this.max;
    }

}
