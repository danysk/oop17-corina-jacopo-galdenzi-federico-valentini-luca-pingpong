package src.model;

import java.awt.Point;
import java.awt.Rectangle;

import src.utilities.Direction;
/**
 * 
 * @author Luca Valentini
 *
 */
public interface Racket {
    /**
     * this method allows the racket to move up or down around the screen.
     * @param dir represents the direction of the racket
     * @return a point (x,y)
     */
    Point move(Direction dir);
    /**
     * get the xUp of the racket.
     * @return xUp of the racket
     */
    int getXUp();
    /**
     * get the yUp of the racket.
     * @return yUp of the racket
     */
    int getYUp();
    /**
     * get the yDown of the racket.
     * @return yDown of the racket
     */
    int getYDown();
    /**
     * get the xDown of the racket.
     * @return xDown of the racket
     */
    int getXDown();
    /**
     * get the height of the racket.
     * @return height of the racket
     */
    int getHeight();
    /**
     * get the width of the racket.
     * @return width of the racket
     */
    int getWidth();
    /**
     * get the boundary of the racket.
     * @return a rectangle
     */
    Rectangle getBoundRacket();
}
