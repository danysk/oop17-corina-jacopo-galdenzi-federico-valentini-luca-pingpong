/*
 * 
 */
package src.model;

import java.util.Optional;

import src.observerpattern.ExpiringMatchTimerObserver;
import src.observerpattern.ObservableExpiringMatchTimer;
import src.observerpattern.ObservableTimer;
import src.observerpattern.TimerObserver;

/**
 * The Class MatchTimer.
 *
 * @author Jacopo Corina.
 * This class represent the MatchTimer in fact extends the Timer class and manage the time of the match.
 */
public class MatchTimer extends Timer implements ObservableTimer, ObservableExpiringMatchTimer {
    private Optional<TimerObserver> observer = Optional.empty();
    private Optional<ExpiringMatchTimerObserver> expiringObserver = Optional.empty();
    /**
     * this is the constructor of MatchTimer class.
     *
     * @param interval is the interval of timer
     * @param duration is the duration of timer
     */
    public MatchTimer(final long interval, final long duration) {
        super(interval, duration);
    }
    /**
     * Instantiates a new match timer.
     *
     * @param interval the interval
     * @param duration the duration
     * @param obs the timer observer
     * @param obs2 the expiring timer observer
     */
    public MatchTimer(final long interval, final long duration, final TimerObserver obs, final ExpiringMatchTimerObserver obs2) {
        super(interval, duration);
        setTimerObserver(obs);
        setExpiringMatchTimerObserver(obs2);
    }
    @Override
    protected final void onTick() {
        notifyTimerObserver();
    }
    @Override
    protected final void onFinish() {
        notifyExpiringMatchTimerObserver();
    }
    @Override
    public final void setTimerObserver(final TimerObserver observer) {
        if (!this.observer.isPresent()) {
            this.observer = Optional.of(observer);
        }
    }
    @Override
    public final void notifyTimerObserver() {
        observer.ifPresent(x -> x.executeOnTimerUpdate(getRemainingTime()));
    }
    @Override
    public final void setExpiringMatchTimerObserver(final ExpiringMatchTimerObserver observer) {
        if (!this.expiringObserver.isPresent()) {
            this.expiringObserver = Optional.of(observer);
        }

    }
    @Override
    public final void notifyExpiringMatchTimerObserver() {
        expiringObserver.ifPresent(x -> x.executeOnMatchTimerExpiration()); 
    }

}
