package src.model;

import src.observerpattern.GraphicBonusShowObserver;
/**
 * 
 * @author Jacopo Corina
 * This class it is used when hasCollision() in CollisionBonus return true and generate a new bonus after the timer finished.
 */
public class BonusGenerationTimer extends Timer {
    private BonusManager bm;
    private GraphicBonusShowObserver obs;
    /**
     * The constructor of BonusGenerationTimer, it also start the timer when it is initializes.
     * @param interval the interval of the timer.
     * @param duration the duration of the timer.
     * @param bm the bonus manager
     * @param obs the observer of the bonus's list in the view, it is used to add a new bonus when the timer finish.
     */
    public BonusGenerationTimer(final int interval, final int duration, final BonusManager bm, final GraphicBonusShowObserver obs) {
        super(interval, duration);
        this.bm = bm;
        this.obs = obs;
        this.start();
    }
    @Override
    protected void onTick() {
    }
    @Override
    protected final void onFinish() {
        if (bm.generateBonus()) {
            obs.addBonus(bm.getLastBonusCreated()); 
        }
    }

}
