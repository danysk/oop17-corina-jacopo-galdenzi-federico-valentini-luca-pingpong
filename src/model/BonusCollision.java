package src.model;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Ellipse2D;
import java.util.List;
import java.util.Optional;

import src.utilities.BonusType;

/**
 * 
 * @author Federico Galdenzi This class provide methods explained in Collision
 *         interface.
 */
public class BonusCollision implements CollisionStrategy {
    private Ball ball;
    //private BonusManager bonusManager;
    private List<Bonus> bonusGeneratedList;
    private Optional<Bonus> hitBonus;

    /**
     * This is the constructor of the BonusCollision class.
     * 
     * @param ball
     *            the Ball object
     * @param bonusGeneratedList
     *            the list of the bonus generated.
     */
    public BonusCollision(final Ball ball, final List<Bonus> bonusGeneratedList) {
        this.ball = ball;
        this.bonusGeneratedList = bonusGeneratedList;
        this.hitBonus = Optional.empty();
    }

    @Override
    public final boolean hasCollision() {
        Rectangle ballBound = new Rectangle(this.ball.getProprierty().getX(), this.ball.getProprierty().getY(),
                this.ball.getProprierty().getDiameter(), this.ball.getProprierty().getDiameter());
        //List<Bonus> copyList = new ArrayList<>(this.bonusManager.getGeneratedBonusList());
        this.hitBonus = bonusGeneratedList.stream()
                .filter(bonus -> getBounds(bonus.getPosition(), bonus.getDiameter()).intersects(ballBound)).findFirst();
        if (hitBonus.isPresent()) {
            /**
             * Se il bonus è presente, mi basta gestire l'applicazione del bonus all'interno del bonus manager
             * questo metodo viene utilizzato all'interno di ballAgentImpl
             * trasporta il metodo applyBonusStrategy in bonusManager
             */
            /**
            this.applyBonusStrategy(hitBonus.get().getType());
            this.bonusManager.deleteGeneratedBonus(hitBonus.get().getPosition());
            Optional.of(hitBonus.get().getPosition());
            */
            return true;
        }
        this.hitBonus = Optional.empty();
        return false;
    }

    private Ellipse2D getBounds(final Point p, final int diameter) {
        return new Ellipse2D.Double(p.x, p.y, diameter, diameter);
    }

    @Override
    public final Optional<Point> getCollisionPoint() {
        return (this.hitBonus.isPresent()) ? Optional.of(this.hitBonus.get().getPosition()) : Optional.empty();
    }

    @Override
    public final boolean canBallMove() {
        return true;
    }

    /**
     * This method return the Bonus type of the bonus took.
     * 
     * @return  empty if there isn't collision, the BonusType if
     *         there is the collision.
     */
    public Optional<BonusType> getHitBonusType() {
        if (this.hitBonus.isPresent()) {
            return Optional.of(this.hitBonus.get().getType());
        }
        return Optional.empty();
    }
    /**
     * This method return the hitBonus field of the class.
     * @return the Bonus hitted.
     */
    public Optional<Bonus> getHitBonus() {
        return this.hitBonus;
    }
}
