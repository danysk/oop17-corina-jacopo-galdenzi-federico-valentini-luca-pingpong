package src.model;

import java.awt.Dimension;
import java.awt.Rectangle;

import src.utilities.Direction;

/**
 * 
 * @author Federico Galdenzi
 * This class rappresents the Ball interface.
 */
public class BallImpl implements Ball {
    private final BallProprierty state;
    private boolean hasScore;
    private int newPointTo;
    /**
     * Instantiates the BallImpl class and also instantiates the BallProprierty class.
     * @param sizeWindow the dimension of the playing field.
     * @param diameter the diameter of the ball.
     */
    public BallImpl(final Dimension sizeWindow, final int diameter) {
        this.state = new BallProprierty(sizeWindow, diameter);
        this.hasScore = false;
        newPointTo = -1;
    }
    /**
     * Another constructor used by JTestUnit package.
     * @param sizeWindow the size of the match window
     * @param diameter the ball diameter
     * @param xSpeed the ball horizontal speed
     * @param ySpeed the ball vertical speed
     */
    public BallImpl(final Dimension sizeWindow, final int diameter, final int xSpeed, final int ySpeed) {
        this.state = new BallProprierty(sizeWindow, diameter);
        this.state.setSpeedX(xSpeed);
        this.state.setSpeedY(ySpeed);
    }
    @Override
    public final void move() {
        if (this.state.getYDirection() == Direction.UP) {
            if (this.state.getY() >= this.state.getBoundaryWindow().getYBoundLaneUp()) {
                this.state.addToPosition(0, this.state.getSpeedWithSignY());
            }
        }
        if (this.state.getYDirection() == Direction.DOWN) {
            if (this.state.getY() <= this.state.getBoundaryWindow().getYBoundLaneDown()) {
                this.state.addToPosition(0, this.state.getSpeedWithSignY());
            }
        }

        if (this.state.getXDirection() == Direction.LEFT) {
            if (this.state.getX() > this.state.getBoundaryWindow().getXBoundLaneLeft()) {
                this.hasScore = false;
                this.state.addToPosition(this.state.getSpeedWithSignX(), 0);
            } else {
                this.hasScore = true;
                this.newPointTo = 2;
            }
        }
        if (this.state.getXDirection() == Direction.RIGHT) {
            if (this.state.getX() < this.state.getBoundaryWindow().getXBoundLaneRight()) {
                hasScore = false;
                this.state.addToPosition(this.state.getSpeedWithSignX(), 0);
            } else {

                this.hasScore = true;
                this.newPointTo = 1;
            }

        }

    }
    @Override
    public final boolean hasScore() {
        return this.hasScore;
    }
    @Override
    public final void resetHasScore() {
        this.state.randomizeDirection();
        this.hasScore = false;
        this.newPointTo = -1;
    }
    @Override
    public final Rectangle getShape() {
        return new Rectangle(this.state.getX(), this.state.getY(), this.state.getDiameter(), this.state.getDiameter());
    }
    @Override
    public final BallProprierty getProprierty() {
        return this.state;
    }
    @Override
    public final int getNewPointTo() {
        return this.newPointTo;
    }

}

