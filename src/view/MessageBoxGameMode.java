package src.view;

import java.awt.Dimension;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import src.model.GameConstant;
/**
 * 
 * @author Jacopo Corina
 * This class defines a message box on which the user can choose the game mode
 *
 */
public class MessageBoxGameMode extends JPanel {
    /**
     * 
     */
    private static final long serialVersionUID = -3476143999897356227L;
    private JRadioButton radio1;
    private JRadioButton radio2;
    private ButtonGroup group;

    /**
     * this is the constructor method of MessageBoxGameMode class.
     */
    public MessageBoxGameMode() {
        radio1 = new JRadioButton("Player vs Player", true);
        radio2 = new JRadioButton("Player vs COM");
        group = new ButtonGroup();
        group.add(radio1);
        group.add(radio2);
        this.add(radio1);
        this.add(radio2);
    }

    @Override
    public final Dimension getPreferredSize() {
        return new Dimension(GameConstant.JOPTIONPANE_WIDTH, GameConstant.JOPTIONPANE_HEIGHT);
    }

    /**
     * 
     * @return 0 if radio1 is selected, 1 otherwise
     */
    public final int getSelected() {
        if (radio1.isSelected()) {
            return 0;
        } else {
            return 1;
        }
    }
}
